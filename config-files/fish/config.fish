
# To include configuration only for login shells, use
# if status is-login
#    ...
# end
# To include configuration only for interactive shells, use
# if status is-interactive
#   ...
# end

### PATH ###
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export GPG_TTY=$(tty)

### ALIASES ###

# sudo as doas
alias sudo ='doas'

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# Changing "ls" to "eza"
alias ll='eza -lah@ --color=always --icons'      # my preferred listing

# Changing "cat" to "bat"
# alias cat='bat'

# pacman and aur helpers
alias pacsyu='doas pacman -Syyu'                 # update only standard pkgs
alias pacs='doas pacman -S --needed'           # install package with pacman
alias unlock='doas rm /var/lib/pacman/db.lck'    # remove pacman lock
alias cleanup='doas pacman -Rns (pacman -Qtdq)'  # remove orphaned packages
alias parsua='paru -Sua --noconfirm'             # update only AUR pkgs (paru)
alias parsyu='paru -Syu --noconfirm'             # update standard pkgs and AUR pkgs (paru)

# portage
# for emerge packages
alias emu='doas emerge -alv --quiet-build=y ' 
# for syncing emerge repos
alias esync='doas emerge --sync'
# updating all packages that requires an update
alias emusyu='doas emerge -auDNlv --quiet-build=y --keep-going=y --with-bdeps=y @world' 
# clean orphans and remove package
alias eclean='doas emerge -acv'
# searching for a package
alias esearch='emerge --search' 

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -I'

# the terminal rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'

# for shutdown
alias kapat='doas poweroff'

# for vi
alias vi='nvim'

### FUNCTIONS ###

# Functions needed for !! and !$
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

# The bindings for !! and !$
if [ $fish_key_bindings = "fish_vi_key_bindings" ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end




### END OF FUNCTIONS ###

### SETTING THE STARSHIP PROMPT ###
#starship init fish | source

