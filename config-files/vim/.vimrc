set encoding=utf-8


""""""""""""""""""
"""" vim-plug """"
""""""""""""""""""

call plug#begin()

Plug 'junegunn/vim-plug'
Plug 'itchyny/lightline.vim'
Plug 'ap/vim-css-color'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'junegunn/fzf.vim'
Plug 'stsewd/fzf-checkout.vim'
Plug 'rust-lang/rust.vim'
Plug 'vim-syntastic/syntastic'
Plug 'tpope/vim-surround'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-fugitive'
Plug 'ayu-theme/ayu-vim'
Plug 'preservim/nerdtree'


Plug 'ryanoasis/vim-devicons'

call plug#end()


""""""""""""""""""""""""""
"""" General Settings """"
""""""""""""""""""""""""""

if exists('+termguicolors')
  let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
"set termguicolors     " enable true colors support
"let ayucolor="light"  " for light version of theme
"let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme
colorscheme ayu
"open NerdTree when vim start
"autocmd VimEnter * NERDTree | wincmd p


filetype on
filetype plugin on
filetype indent on
syntax on
set nu
set rnu
set shiftwidth=4
set tabstop=4 softtabstop=4
set expandtab
set nobackup
set nowritebackup
set smartindent
set scrolloff=10
set nowrap
set incsearch
set ignorecase
set smartcase
set showcmd
set showmode
set showmatch
set nohlsearch
set history=1000
set laststatus=2
set hidden
set noerrorbells
set signcolumn=yes
set noswapfile

"""""""""""""""""
"""" Keymaps """"
"""""""""""""""""

let mapleader = " "
nnoremap <F3> :set relativenumber! <CR>
nnoremap <F4> :NERDTree<CR>
nnoremap <leader>n :NERDTreeToggle<CR>
" Centering after <C-d> and <C-u>
nnoremap <C-d> <C-d>zz
nnoremap <C-u> <C-u>zz
" for navigate between windows without ctrl-w
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
" for fugitive
nmap <leader>gs :G<CR>
nmap <leader>gc :GCheckout<CR>
"for CoC
inoremap <expr> <cr> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"
inoremap <expr> <Tab> coc#pum#visible() ? coc#pum#next(1) : "\<Tab>"
inoremap <expr> <S-Tab> coc#pum#visible() ? coc#pum#prev(1) : "\<S-Tab>"
