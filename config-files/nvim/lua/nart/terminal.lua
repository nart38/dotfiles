-- Function to open a terminal in a floating window
local function floating_terminal()
  local ui = vim.api.nvim_list_uis()[1]
  local height = math.ceil(ui.height * 0.6)
  local width = math.ceil(ui.width * 0.6)
  local conf = {
    relative = "editor",
    anchor = "NW",
    width = width,
    height = height,
    row = math.ceil((ui.height - height) / 2),
    col = math.ceil((ui.width - width) / 2),
    style = "minimal",
    border = "rounded",
  }

  local new_buf = vim.api.nvim_create_buf(true, false)
  local new_win = vim.api.nvim_open_win(new_buf, true, conf)
  -- vim.api.nvim_win_set_option(new_win, "winhl", "Normal:Normal,FloatBorder:Normal")
  vim.api.nvim_command("terminal")
  vim.api.nvim_command("startinsert")

  vim.api.nvim_buf_set_keymap(
    new_buf,
    'n',
    "<ESC>",
    ":q!<CR>",
    {})
end

-- Create a ':FTerm' command to open terminal.
vim.api.nvim_create_user_command(
  'FTerm',
  function ()
    floating_terminal()
  end,
  { desc = "Open a floating terminal window."}
)

vim.keymap.set(
  'n',
  "<leader><Enter>",
  ":FTerm<CR>",
  { desc = "Open a floating terminal window." })

vim.keymap.set(
  't',
  "<ESC>",
  "<C-\\><C-n>",
  { desc = "Set ESC for going back from terminal mode to normal." })
