#! /bin/sh

menu="fzf -m --reverse --border=top"
selections=$(ls ./config-files | $menu)
for file in $selections; do
    echo "Creating link for: $file... "
    ln -s -f $(pwd)/config-files/$file $HOME/.config &&
    echo "Success."
done
